#!/bin/sh
# First just query the resolutions which seems to let X11 discovers new
# resolutions.  Then set the resolution via --auto.
xrandr -display :0
xrandr -display :0 --output virtual --auto
