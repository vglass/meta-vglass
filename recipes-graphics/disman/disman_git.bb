DESCRIPTION = "Display Manager"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=33f1e9b996445fae3abdec0dc53f884f"

DEPENDS = " \
    dbus \
    qtbase \
    virtual/libivc \
    libpvglass \
    vglass \
"

PV = "0+git${SRCPV}"
SRC_URI = " \
    git://gitlab.com/vglass/disman.git;protocol=https;branch=master \
    file://disman-monit \
"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

PACKAGECONFIG ??= ""
PACKAGECONFIG[debug] += "CONFIG+=debug,CONFIG+=release DEFINES+=NDEBUG"

require recipes-qt/qt5/qt5.inc
inherit update-rc.d

EXTRA_QMAKEVARS_PRE += "${PACKAGECONFIG_CONFARGS}"

INITSCRIPT_NAME = "disman"
INITSCRIPT_PARAMS = "start 99 5 . stop 1 0 1 2 3 4 6 ."

PACKAGE_BEFORE_PN += "${PN}-monit"
# Not using -tools pkg-split.
PACKAGES_remove += "${PN}-tools"
FILES_${PN} += " \
    ${OE_QMAKE_PATH_BINS} \
"
FILES_${PN}-monit += " \
    ${sysconfdir}/monit.d \
"
RRECOMMENDS_${PN} += " \
    ${PN}-monit \
"
RDEPENDS_${PN} += " \
    libpvbackendhelper \
    bash \
"
RDEPENDS_${PN}-monit += " \
    monit \
"

do_install_append() {
    install -d ${D}${sysconfdir}/monit.d
    install -m 0644 ${WORKDIR}/disman-monit ${D}${sysconfdir}/monit.d/disman
}
