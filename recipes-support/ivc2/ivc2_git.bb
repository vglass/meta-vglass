SUMMARY = "libivc specific for dom0"
DESCRIPTION = "This recipe builds/provides libivc for dom0."
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://../../../LICENSE;md5=33f1e9b996445fae3abdec0dc53f884f"

DEPENDS = " \
    qtbase \
    libxenbe \
"

PV = "0+git${SRCPV}"
SRC_URI = " \
    git://gitlab.com/vglass/ivc.git;protocol=https;branch=master \
    file://ivcdaemon.initscript \
    file://ivcdaemon-monit \
"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/src/usivc/ivcdaemon"

PACKAGECONFIG ??= ""
PACKAGECONFIG[debug] += "CONFIG+=debug,CONFIG+=release DEFINES+=NDEBUG"

require recipes-qt/qt5/qt5.inc
inherit update-rc.d

EXTRA_QMAKEVARS_PRE += "${PACKAGECONFIG_CONFARGS}"

do_install_append() {
    install -d "${D}${sysconfdir}/init.d"
    install -m 755 "${WORKDIR}/ivcdaemon.initscript" "${D}${sysconfdir}/init.d/ivcdaemon"

    install -d ${D}${sysconfdir}/monit.d
    install -m 0644 ${WORKDIR}/ivcdaemon-monit ${D}${sysconfdir}/monit.d/ivcdaemon
}

INITSCRIPT_NAME = "ivcdaemon"
INITSCRIPT_PARAMS = "defaults 97 0"

PACKAGE_BEFORE_PN += "${PN}-monit"
# Not using -tools pkg-split.
PACKAGES_remove += "${PN}-tools"
FILES_${PN} += " \
    ${OE_QMAKE_PATH_BINS} \
"
FILES_${PN}-monit += " \
    ${sysconfdir}/monit.d \
"

RRECOMMENDS_${PN} += " \
    ${PN}-monit \
"

RDEPENDS_${PN}-monit += " \
    monit \
"
